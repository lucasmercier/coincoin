package com.example.coincoin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.connect:

                //Creates variables with our user input data
                EditText editText = (EditText)findViewById(R.id.username);
                String username = editText.getText().toString();

                editText = (EditText)findViewById(R.id.password);
                String password = editText.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("username",username);
                intent.putExtra("password",password);

                setResult(1,intent);
                finish();
                break;
        }
    }
}
