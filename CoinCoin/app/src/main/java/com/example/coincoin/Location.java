package com.example.coincoin;

import androidx.annotation.NonNull;

final class Location {
    private String name;
    private double longitude, latitude;
    private int id;
    private float note;
    private static int idCounter = 0;

    public Location(String name, double latitude, double longitude, float note) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.note = note;
        this.id = idCounter++;
    }


    /* GETTERS AND SETTERS */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }

    public int getId() {
        return id;
    }

    @NonNull
    @Override
    public String toString() {
        return "Name :" + this.name + ", la:" + this.latitude + ", lo:" + this.longitude + ", note:" + this.note;
    }
}
