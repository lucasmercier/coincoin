package com.example.coincoin;

import java.util.Date;

final class CustomImage {

    private String file;
    private int location_id;
    private Date date;

    public CustomImage(String file, int location_id, Date date) {
        this.file = file;
        this.location_id = location_id;
        this.date = date;
    }

    /* GETTERS SETTERS */

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
