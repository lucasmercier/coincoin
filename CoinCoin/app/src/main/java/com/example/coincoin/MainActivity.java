package com.example.coincoin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity{

    private static boolean connected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Reads if user already logged an account
        File file = this.getFileStreamPath("userdata.txt");
        if(file.exists()){
            String data = readFromFile(this, "userdata.txt");
            String[] userdata = data.split(" & ");
            if(!userdata[0].equals("") && !userdata[1].equals("")){
                connected = true;
            }
        }
        if(!connected){
            startActivityForResult(new Intent(MainActivity.this, LoginActivity.class),1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(connected){
            startActivity(new Intent(MainActivity.this, MapsActivity.class));
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                switch (resultCode) {
                    case 1:
                        if (data != null) {
                            String username = data.getStringExtra("username"),
                                    password = data.getStringExtra("password");
                            connected = true;
                            writeToFile("userdata.txt",username +" & "+password,this);
                            //A confirmation message to show that the Contact as been added
                            Toast.makeText(this, "Connecté !" , Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }


    private void writeToFile(String name, String data, Context context) {
        try {
            //Writes in contact.txt the data
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(name, Context.MODE_APPEND));
            outputStreamWriter.write(data);

            //Adds a separator the distinguish our differents datas
            outputStreamWriter.write(" && ");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Toast.makeText(this, "File write failed"+ e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private String readFromFile(Context context, String name) {

        String ret = "";

        try {

            //Opens the specified file
            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                //Reads the file
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                //Writes in a response string the file's data
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Toast.makeText(this, "File not found "+ e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, "Can not read file "+ e.toString(), Toast.LENGTH_LONG).show();
        }

        return ret;
    }

}
