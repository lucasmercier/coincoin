-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 20 déc. 2019 à 09:57
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `coincoins`
--

-- --------------------------------------------------------

--
-- Structure de la table `lieux`
--

DROP TABLE IF EXISTS `lieux`;
CREATE TABLE IF NOT EXISTS `lieux` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `valide` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lieux`
--

INSERT INTO `lieux` (`id`, `nom`, `latitude`, `longitude`, `valide`) VALUES
(1, 'Etretat', 49.7, 0.2, 1),
(2, 'Les gorges du verdon', 43.7613, 6.3788, 1),
(3, 'Mont Saint-Michel', 48.636, -1.5114, 1),
(4, 'Mont blanc', 6.86517, 45.8326, 1),
(5, 'Pic du Canigou', 42.5189, 2.45656, 1);

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lien` varchar(1000) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `id_utilisateurs` int(11) NOT NULL,
  `id_lieux` int(11) NOT NULL,
  `valide` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement de données exemples de la table `photos`
--

INSERT INTO `photos` (`id`, `lien`, `latitude`, `longitude`, `id_utilisateurs`, `id_lieux`, `valide`) VALUES
(1, '', 49.7, 0.2, 1, 1, 1),
(2, '', 1, 1, 1, 2, 1),
(3, 'https://cdn.civitatis.com/francia/mont-saint-michel/galeria/mont-saint-michel.jpg', 1, 1, 1, 3, 1),
(4, '', 1, 1, 1, 1, 1),
(5, 'https://www.tourisme.fr/images/otf_destinations/223/entree-des-gorges-du-verdon.jpg', 1, 1, 1, 2, 1),
(6, 'http://www.histoire-normandie.fr/wp-content/uploads/2018/05/laurid.jpg', 1, 1, 1, 3, 1),
(7, 'https://www.altituderando.com/local/cache-vignettes/L1200xH688/randoon5166-51647.jpg', 1, 1, 1, 5, 1),
(8, 'https://i0.wp.com/madeinperpignan.com/wp-content/uploads/2019/08/Jimmy-Phan-La-Course-du-Canigou-2.jpg?ssl=1', 1, 1, 1, 5, 1),
(9, 'https://webgenery-images.s3.eu-west-1.amazonaws.com/5BC458428821C972555846/espace_dynamique/1/7-A.jpg', 1, 1, 1, 4, 1),
(10, 'http://thumbor-prod-eu-central-1.photo.aws.arc.pub/b8ELckZq5UNnRLzA6V772jCZERM=/arc-anglerfish-eu-central-1-prod-leparisien/public/367CH6VUXGZMPLXK4S3ZOGNPBA.jpg', 1, 1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(50) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
